-- Adminer 4.1.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `pagerank`;
CREATE TABLE `pagerank` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `iteration` int(11) NOT NULL,
  `pagerank` double NOT NULL,
  `index_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pagerank` (`pagerank`),
  KEY `index_id` (`index_id`),
  KEY `iteration` (`iteration`),
  CONSTRAINT `pagerank_ibfk_1` FOREIGN KEY (`index_id`) REFERENCES `vwm_index` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


DROP TABLE IF EXISTS `vwm_index`;
CREATE TABLE `vwm_index` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(4096) COLLATE utf8_czech_ci NOT NULL,
  `data` longtext COLLATE utf8_czech_ci,
  `title` longtext COLLATE utf8_czech_ci,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `data` (`data`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;


-- 2015-04-29 08:16:34
