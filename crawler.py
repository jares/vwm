	#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
import re, urllib2
import sys
from HTMLParser import HTMLParser
import MySQLdb
import chardet
import urlparse
import threading
import time
import hashlib
import re
from urlparse import urljoin

coef = 0.85;
limit = 20000;

map = {
	"http://najisto.centrum.cz": True
}
stack = []
pages = {}

stack.append("http://najisto.centrum.cz");

lock = threading.Lock()
counterLock = threading.Lock()


def urlEncodeNonAscii(b):
    return re.sub('[\x80-\xFF]', lambda c: '%%%02x' % ord(c.group(0)), b)

def iriToUri(iri):
    parts= urlparse.urlparse(iri)
    return urlparse.urlunparse(
        part.encode('idna') if parti==1 else urlEncodeNonAscii(part.encode('utf-8'))
        for parti, part in enumerate(parts)
    )


class Page:
	def __init__(self, url):
		self.url = url;
		self.outlinks = [];
		self.inlinks = [];
		self.pageRank = 0;
		self.newPagerank = 0;

	def calculate(self, V, d, s):
		for page in self.inlinks:
			self.newPagerank += page.pageRank / page.realOutlinks
		self.newPagerank = ((1 - d + s) * (1 / V)) + d * self.newPagerank

	def reset(self):
		self.pageRank = self.newPagerank
		self.newPagerank = 0




class MyHTMLParser(HTMLParser):
    def handle_starttag(self, tag, attrs):
    	self.lastTag = tag;
    	if tag == "a":
			for i in attrs:
				if i[0] == "href":
					url = i[1];

					try:
						url = urljoin(self.root, url)

						url = re.sub(r'#.*', '', url)

						res = urlparse.urlparse(url);
					
						if not res.netloc.startswith("najisto.centrum.cz"):
							continue

						url = url.strip();
					except:
						continue

					self.page.outlinks.append(url);

					if not url in map:
						lock.acquire()
						stack.append(url);
						map[url] = True
						lock.release()
					break

    def handle_data(self, data):
        self.actualData += " " + data;
        if self.lastTag == "title" and not self.titleRead:
        	self.titleRead = True
        	self.title = data

    def __init__(self, root, page):
    	self.actualData = ""
    	self.root = root
    	self.page = page
    	self.lastTag = "";
    	self.title = "";
    	self.titleRead = False
    	HTMLParser.__init__(self)


counter = 0;


class CrawlerThread(threading.Thread):
	def __init__(self, id):
		super(CrawlerThread, self).__init__()
		self.threadId = id
         # Open database connection
		self.db = MySQLdb.connect("localhost","root","","vwm" )

		# prepare a cursor object using cursor() method
		cursor = self.db.cursor()
		self.cursor = cursor

	def run(self):
		global counter;
		global limit;
		while(len(stack)):

			if counter > limit:
				break


			lock.acquire()
			url = stack.pop(0);
			lock.release()

			#print self.threadId,  " - require ", url;
			try:
		   		response = urllib2.urlopen(iriToUri(url));
			except urllib2.URLError as e:
		   		continue;
		   	actualData = "";
			html = unicode(response.read(), "utf-8", errors='ignore')

			page = Page(url);

			parser = MyHTMLParser(url, page)


			parser.feed(html);

			sql = """INSERT INTO vwm_index(url, data, title)
		         VALUES (%s, %s, %s)"""

			counterLock.acquire()
			counter += 1;
			mod = counter % 100;
			counterLock.release()
			if not mod:
				print "[INFO] ", counter, " pages completed"

			content = re.sub(r'\s+', ' ', parser.actualData)

			try:
				self.cursor.execute(sql, (url, content[:8000], parser.title))
				page.id = self.db.insert_id()
				self.db.commit()
			except:
				print "db error"
				self.db.rollback()
				#stack.append(url);
				continue

			lock.acquire()
			pages[url] = page
			lock.release()

threads = [];

for i in range(15):
	print i;
	time.sleep(2)
	thread = CrawlerThread(i);
	thread.start();
	threads.append(thread);

for thread in threads:
	thread.join();

initialPagerank = 1 / len(pages)
totalPages = len(pages);

print initialPagerank;

for i in pages:
	outlinks = set(pages[i].outlinks)
	realOutlinks = 0;
	for url in outlinks:
		if url in pages:
			pages[url].inlinks.append(pages[i]);
			realOutlinks += 1
	pages[i].realOutlinks = realOutlinks;
	pages[i].pageRank = initialPagerank;


db = MySQLdb.connect("localhost","root","","vwm" )
cursor = db.cursor()

for iteration in range(70):
	print "[INFO] processing iteration ", iteration

	s = 0;
	for i in pages:
		if pages[i].realOutlinks == 0:
			s += pages[i].pageRank * coef

	for i in pages:
		pages[i].calculate(totalPages, coef, s);

	for i in pages:
		cursor.execute("""INSERT INTO pagerank(iteration, pagerank, index_id) VALUES(%s, %s, %s)""", (iteration, pages[i].newPagerank, pages[i].id))
		db.commit()
		pages[i].reset();















