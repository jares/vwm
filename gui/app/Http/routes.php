<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Input;

$app->get('/', function () {
	return view('default');
});

$app->get('/search', function () use ($app) {
	$iteration = Input::get('iterations');
	$query = Input::get('query');

	$data = DB::select('SELECT *
						FROM vwm_index
						LEFT JOIN pagerank ON vwm_index.id = pagerank.index_id AND pagerank.iteration = ?
						WHERE MATCH (data) AGAINST (?)
						ORDER BY pagerank DESC
						LIMIT 50', [$iteration, $query]);

	foreach ($data as $row) {
		$clear = remove_accents($row->data);
		preg_match_all('/(.{0,150}' . $query . '.{0,150})/i', $clear, $matches);
		if (!empty($matches[0][0])) {
			$row->match = $matches[0][0];
		}
	}

	return $data;
});
