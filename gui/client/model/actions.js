import {dispatch} from '../dispatcher'

export default {
    setHaystack(haystack){
        dispatch(this.setHaystack, haystack);
    },

    setIterations(num){
        dispatch(this.setIterations, num);
    }
}
