import Actions from './actions'
import {register} from '../dispatcher'
import {searchCursor} from '../state'

function reload(){
    let data = searchCursor().get('haystack');
    let iterations = searchCursor().get('iterations');
    if (data.length > 2) {
        $.ajax({
            url: '/search',
            data: {
                query: data,
                iterations: iterations
            }
        }).done(function (payload) {
            searchCursor(search => search.set('results', payload));
        })
    }
}

export const dispatchToken = register(({action, data}) => {
    switch (action) {
        case Actions.setHaystack:
            searchCursor(search => search.set('haystack', data));
            reload();
            break;

        case Actions.setIterations:
            searchCursor(search => search.set('iterations', data));
            reload();
            break;
    }

});

export default {
    getHaystack() {
        return searchCursor().get('haystack');
    },

    getResults() {
        return searchCursor().get('results') || [];
    }
}
