import EventEmitter from 'eventemitter3'
import Immutable from 'immutable'
import React from 'react/addons'


class State extends EventEmitter {

    constructor(state) {
        this._state = null
        this.load(state || {})
    }

    load(state:Object) {
        this.set(Immutable.Map.isMap(state)
            ? state
            : Immutable.fromJS(state))
    }

    set(state) {
        if (this._state === state) {
            return
        }

        this._state = state
        this.emit('change', this._state)
        this.save();
    }

    save():Object {
        localStorage.setItem('state', JSON.stringify(this._state.toJS()));
    }

    toConsole() {
        console.log(JSON.stringify(this.save()))
    }

    cursor(path) {
        return (update) => {
            if (update) {
                this.set(this._state.updateIn(path, update))
            } else {
                return this._state.getIn(path)
            }
        }
    }

}

var cur = {
    search: {
        haystack: '',
        results: [],
        iterations: 1
    }
};


const state = new State(cur);

export default state
export const searchCursor = state.cursor(['search']);
