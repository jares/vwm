import React from 'react'
import state from '../state'
import Store from '../model/store'
import Actions from '../model/actions'
import Result from './result'

export default React.createClass({

    componentDidMount() {
        state.on('change', () => {
            console.time('whole app re-rendered')
            this.forceUpdate(() => {
                console.timeEnd('whole app re-rendered')
            })
        })
    },

    onChange(e) {
        Actions.setHaystack(e.target.value);
    },

    renderResult(result, k) {
        return (
            <Result key={k} data={result} />
        )
    },

    changeIterations(e) {
        Actions.setIterations(e.target.value);
    },

    render() {
        let results = Store.getResults();

        let options = []
        for (let i = 1; i < 70; i++) {
            options.push((
                <option value={i}>{i} iterations</option>
            ))
        }

        return (
            <div>
                <h1>PHP fulltext search engine
                    <select onChange={this.changeIterations}>{options}</select>
                </h1>
                <div>
                    <input value={Store.getHaystack()} type="text" onChange={this.onChange} />
                </div>
            {results.map(this.renderResult)}
            </div>
        )
    }

})
