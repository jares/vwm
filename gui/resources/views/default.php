<!doctype html>
<html>
<head>
	<link href='http://fonts.googleapis.com/css?family=Sanchez:400italic,400&subset=latin,latin-ext' rel='stylesheet'
		  type='text/css'>
	<style>
		body {
			font-family: 'Sanchez', serif;
		}

		input {
			width: 500px;
			height: 40px;
			display: block;
			margin: 0px auto;
			font-size: 20px;
		}

		h1 {
			text-align: center;
		}
	</style>
</head>
<body>
<div id="app"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="js/build.js"></script>
</body>
</html>
