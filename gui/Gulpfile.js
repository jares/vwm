var gulp = require('gulp');
var browserify = require('gulp-browserify');
var rename = require('gulp-rename');
var watch = require('gulp-watch');

gulp.task('default', function () {
    return gulp.src('client/index.jsx', { read: false })
        .pipe(browserify({
            transform: [require('babelify')],
            extensions: ['.js', '.jsx']
        }))
        .pipe(rename('build.js'))
        .pipe(gulp.dest('public/js'));
});

gulp.task('watch', function () {
    watch(['client/**/*.js', 'client/**/*.jsx'], function () {
        gulp.start('default')
    });
});
